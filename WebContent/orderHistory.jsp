<%@ page import="hibernate.shop.Order" %>
<%@ page import="hibernate.shop.OrderRepository" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Item - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<%@include file="head.jsp" %>


<%
    String email;
    //userFromCookie from head.jsp
    if (userFromCookie.isPresent()){
        email = userFromCookie.get().getEmail();
        List<Order> userOrders = OrderRepository.findAllByEmail(userFromCookie.get().getId(), 0);
        pageContext.setAttribute("userOrders", userOrders);
/*
        HashMap<Long, OrderHistory> orderHistoryMap = new HashMap<>();
        for (Order order : userOrders){
            orderHistoryMap.put(order.getId(),
                    order.getOrderHistorySet().stream().sorted((o1, o2) -> o1.getId().compareTo(o2.getId())).findFirst()
            .orElse(new OrderHistory())
            );
        }
*/
    }
%>



<!-- Page Content -->
<div class="container">

    <div class="row">

        <div class="col-lg-3">
            <h1 class="my-4">Shop Name</h1>
            <div class="list-group">
                <a href="#" class="list-group-item active">Category 1</a>
                <a href="#" class="list-group-item">Category 2</a>
                <a href="#" class="list-group-item">Category 3</a>
            </div>
        </div>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Numer zamówienia</th>
                        <th scope="col">Data</th>
                        <th scope="col">Status</th>
                        <th scope="col">Kwota netto</th>
                        <th scope="col">Kwota brutto</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${userOrders}" var="order" varStatus="lp">
                        <tr>
                            <th scope="row">${lp.index+1}</th>
                            <td><a href="/order.jsp?orderId=${order.id}">${order.id}</a></td>
                            <td>${order.currentOrderHistory.statusDateTime}</td>
                            <td>${order.currentOrderHistory.orderStatus}</td>
                            <td>${order.totalNet}</td>
                            <td>${order.totalGross}</td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
            <!-- /.card -->


        </div>
        <!-- /.col-lg-9 -->

    </div>

</div>
<!-- /.container -->

<!-- Footer -->
<%@include file="footer.jsp" %>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
