<%@ page import="hibernate.hibernate.util.UserSessionHelper" %>
<%@ page import="hibernate.shop.User" %>
<%@ page import="java.util.Optional" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Michał
  Date: 2018-04-14
  Time: 13:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%
    Optional<User> userFromCookie = UserSessionHelper.getUserFromCookie(request.getCookies());

    if (userFromCookie.isPresent()) {
        pageContext.setAttribute("user", userFromCookie.get());
    }
    //userFromCookie.ifPresent(user -> pageContext.setAttribute("user", user));

    Object i = session.getAttribute("i");
    if (i == null){
        session.setAttribute("i", 1);
    } else {
        session.setAttribute("i", (int) i+1);
    }

    pageContext.setAttribute("i", session.getAttribute("i"));
    //application - for all users the same
%>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="/index.jsp">Super sklep</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link">Licznik: ${i}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/about.jsp">O sklepie</a>
                </li>
                <c:if test="${user == null}">
                    <li class="nav-item">
                        <a class="nav-link" href="/login.jsp">Zaloguj</a>
                    </li>
                </c:if>
                <c:if test="${user != null}">
                    <li class="nav-item">
                        <a class="nav-link" href="/cart.jsp">Koszyk.</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link">Witaj, ${user.firstName} ${user.lastName}.</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/orderHistory.jsp">Twóje zamówienia</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout">Wyloguj</a>
                    </li>
                </c:if>
            </ul>
        </div>
    </div>
</nav>
