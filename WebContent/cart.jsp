<%@ page import="hibernate.shop.Cart" %>
<%@ page import="hibernate.shop.CartRepository" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Shop Item - Start Bootstrap Template</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <!-- Custom styles for this template -->
    <link href="css/shop-homepage.css" rel="stylesheet">

</head>

<body>

<!-- Navigation -->
<%@include file="head.jsp" %>

<!-- Page Content -->
<div class="container">

    <div class="row">

        <!--Left Menu -->
        <%@include file="leftMenu.jsp" %>

        <%
            if (userFromCookie.isPresent()) {
                Optional<Cart> cartByUserId = CartRepository.findByUserId(userFromCookie.get().getId());
                boolean isEmpty = true;
                if (cartByUserId.isPresent()) {
                    if (!cartByUserId.get().isEmpty()) {
                        isEmpty = false;
                    }
                    pageContext.setAttribute("cart", cartByUserId.get());
                }
                pageContext.setAttribute("isEmpty", isEmpty);
            }
        %>
        <!-- /.col-lg-3 -->

        <div class="col-lg-9">
            <h2>Koszyk</h2>
            <c:if test="${isEmpty}">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nazwa produktu</th>
                        <th scope="col">Ilość</th>
                        <th scope="col">Kwota netto</th>
                        <th scope="col">Kwota brutto</th>
                        <th scope="col">Razem netto</th>
                        <th scope="col">Razem brutto</th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>${item.price.grossPrice.multiply(item.amount)}</td>
                            <td colspan="6"><h3>Twój koszyk jest pusty. Dodaj jakiś produkt :-)</h3></td>
                        </tr>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${!isEmpty}">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th scope="col">Nazwa produktu</th>
                        <th scope="col">Ilość</th>
                        <th scope="col">Kwota netto</th>
                        <th scope="col">Kwota brutto</th>
                        <th scope="col">Razem netto</th>
                        <th scope="col">Razem brutto</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${cart.cartItemSet}" var="item" varStatus="it">
                        <tr>
                            <th scope="row">
                                <a href="/product.jsp?productId=${product.id}">${product.name}</a>
                            </th>
                            <td>
                                <input name="amount_1" value="${item.amount}"/>
                                <span class="glyphicon glyphicon-plus" aria-hidden="true">
                                    +
                                    -
                                </span>

                            </td>
                            <td>${item.price.netPrice}</td>
                            <td>${item.price.grossPrice}</td>
                            <td>${item.price.netPrice.multiply(item.amount)}</td>
                            <td>${item.price.grossPrice.multiply(item.amount)}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th></th>
                        <td></td>
                        <td></td>
                        <td><b>Suma:</b></td>
                        <td>${cart.totalNet} zł</td>
                        <td>${cart.totalGross} zł</td>
                    </tr>
                    </tfoot>
                </table>
            </c:if>
            <!-- /.card -->

            <div class="row">
                <div class="col-md-6">
                    <h5>Metoda dostawy:</h5>
                    <select class="form-control">
                        <option>Kurier</option>
                        <option>Odbiór osobisty</option>

                    </select>
                </div>
                <div class="col-md-6">
                    <h5>Adres dostawy</h5>
                    <div class="col-md-12">
                        <label>Miejscowość</label>
                        <input type="text" class="form-control" name="city"/>
                    </div>
                    <div class="col-md-12">
                        <label>Kod pocztowy</label>
                        <input type="zipCode" class="form-control" name="city"/>
                    </div>
                    <div class="col-md-12">
                        <label>Ulica</label>
                        <input type="street" class="form-control" name="city"/>
                    </div>

                </div>
            </div>

            <div>
                <%--<button class="btn btn-success">Kup i zapłać</button>--%>
                <a href="/createOrder" class="btn btn-success">Kup i zapłać</a>
            </div>

        </div>
        <!-- /.col-lg-9 -->

    </div>

</div>
<!-- /.container -->

<!-- Footer -->
<%@include file="footer.jsp" %>

<!-- Bootstrap core JavaScript -->
<script src="vendor/jquery/jquery.min.js"></script>
<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
