package hibernate.hibernate.util;

import org.hibernate.Session;

/*
    create 2018-03-17 21:45 by Michał
*/

//TODO add interface for all classes uses method from this class

/**
 * Utility class to make common saving method
 * @param <E> generic type of object to save into db
 */
public class RepositoryUtil<E> {

    /**
     * Generic method to save any type of object into db
     * @param element generic type object to save into db
     */
    public void saveObjectIntoDB(E element) {

        Session session = null;
        try {
            session = HibernateUtil.openSession();
            session.getTransaction().begin();
            session.saveOrUpdate(element);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction().isActive()) {
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

}
