package hibernate.shop;

import hibernate.hibernate.util.HibernateUtil;
import hibernate.hibernate.util.RepositoryUtil;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.math.BigDecimal;
import java.util.Optional;

/*
    create 2018-03-17 10:28 by Michał
*/
public class CartRepository {

    public static void saveCartIntoDB(Cart cart) {
        RepositoryUtil<Cart> cartRepositoryUtil = new RepositoryUtil<>();
        cartRepositoryUtil.saveObjectIntoDB(cart);
    }

    public static Optional<Cart> findByUserId(Long userId) {
        //TODO should return new, empty cart, if none exist (would brake single responsibility principle
        Session session = null;
        try {
            session = HibernateUtil.openSession();
            String jpql = "SELECT c FROM Cart c WHERE c.user.id = :userId";
            Query query = session.createQuery(jpql);
            query.setParameter("userId", userId);
            return Optional.ofNullable((Cart) query.getSingleResult());
        } catch (Exception e) {
            e.printStackTrace();
            return Optional.empty();
        } finally {
            if (session != null && session.isOpen()) {
                session.close();
            }
        }
    }

    /**
     * Create new cart item to add into cart
     *
     * @param productId     id of product to add
     * @param productAmount amount of product to add
     * @param cart          user's shop cart
     */
    public static boolean createNewCartItem(Cart cart, Long productId, BigDecimal productAmount) {
        //TODO parsing check
        Optional<Product> productById = ProductRepository.findOneById(productId);
        if (productById.isPresent()) {
            CartItem item = new CartItem();
            item.setAmount(productAmount);
            item.setProduct(productById.get());
            item.setPrice(productById.get().getPrice());
            cart.addCartItem(item);
            return true;
        } else {
            return false;
        }
    }

    public static void removeCart(Cart cart) {
        Session session = null;
        try {
            session = HibernateUtil.openSession();
            session.getTransaction().begin();
            session.remove(cart);
            session.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
            if (session.getTransaction().isActive()){
                session.getTransaction().rollback();
            }
        } finally {
            if (session != null && session.isOpen()){
                session.close();
            }
        }
    }
}
