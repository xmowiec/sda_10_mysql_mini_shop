package hibernate.shop;

import hibernate.hibernate.util.RepositoryUtil;

public class OrderComplaintRepository {

    public static void saveOrderComplaintIntoDB(OrderComplaint ordersComplaint) {
        RepositoryUtil<OrderComplaint> productRepositoryUtil = new RepositoryUtil<>();
        productRepositoryUtil.saveObjectIntoDB(ordersComplaint);
    }

}
