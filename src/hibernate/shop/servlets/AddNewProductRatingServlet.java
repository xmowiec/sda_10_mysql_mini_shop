package hibernate.shop.servlets;

import hibernate.hibernate.util.UserSessionHelper;
import hibernate.shop.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Optional;

/*
    create 2018-04-14 11:20 by Michał
*/

@WebServlet(name = "AddNewProductRatingServlet", urlPatterns = "/addNewProductRating")
public class AddNewProductRatingServlet extends HttpServlet {

    private static final int MIN_RATING = 1;
    private static final int MAX_RATING = 5;

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Optional<User> userFromCookie = UserSessionHelper.getUserFromCookie(req.getCookies());
        String description = req.getParameter("description");
        Optional<Product> product = ProductRepository.findOneById(MathUtil.parseStringToLong(req.getParameter("productId")));
        double rating = MathUtil.parseStringToDouble(req.getParameter("rating"));


        if (product.isPresent() && userFromCookie.isPresent() && ratingIsValid(rating)) {
            ProductRating productRating = ProductRating.builder()
                    .addTimeStamp(LocalDateTime.now())
                    .description(description)
                    .rating(rating)
                    .user(userFromCookie.get())
                    .product(product.get())
                    .build();
            ProductRatingRepository.saveProductRatingIntoDB(productRating);
        }
        req.getRequestDispatcher("/product.jsp?productId=" + product.get().getId()).forward(req, resp);
    }


    private boolean ratingIsValid(double rating) {
        return MIN_RATING <= rating && rating <= MAX_RATING;
    }
}