package hibernate.shop.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Optional;

/*
    create 2018-04-10 18:41 by Michał
*/
public class LogoutServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        Cookie[] cookies = req.getCookies();
        String result = null;
        if (cookies != null) {
            Optional<Cookie> cookieFromEmail = Arrays.stream(cookies)
                    .filter(cookie -> cookie.getName().equals("email")).findFirst();
            if (cookieFromEmail.isPresent()) {
                Cookie cookie = cookieFromEmail.get();
                cookie.setMaxAge(0); //delete cookie right now
                resp.addCookie(cookie);
                PrintWriter writer = resp.getWriter();
                result = "Zostałeś wylogowany!";
                writer.write(result);
            }
        }
//TODO change String into array of String
//        req.getParameterMap().put("info", result);
        req.getRequestDispatcher("/index.jsp?isSuccessLogout=true").forward(req, resp);
                }
                }
