package hibernate.shop.servlets;

import hibernate.shop.User;
import hibernate.shop.UserRepository;
import lombok.ToString;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;

/*
    create 2018-04-10 10:54 by Michał
*/
@ToString
public class LoginServlet extends HelloServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        System.out.println(this.toString());

        Optional<User> userByEmailAndPassword = UserRepository.findByEmailAndPassword(email, password);

        if (userByEmailAndPassword.isPresent()) {
            PrintWriter writer = resp.getWriter();
            resp.addCookie(new Cookie("email", email));
            writer.write("Zalogowano użytkownika o adresie email: " + email);
            System.out.println("Zalogowano z id: " + userByEmailAndPassword.get().getId());
        }
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
