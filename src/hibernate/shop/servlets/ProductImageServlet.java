package hibernate.shop.servlets;

import hibernate.shop.MathUtil;
import hibernate.shop.Product;
import hibernate.shop.ProductRepository;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/*
    create 2018-04-21 11:22 by Michał
*/

@WebServlet(name = "ProductImageServlet", urlPatterns = "/productImage")
public class ProductImageServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Long productId = MathUtil.parseStringToLong(req.getParameter("productId"));
        Optional<Product> productById = ProductRepository.findOneById(productId);
        if (productById.isPresent()){
            resp.setHeader("Content-Length", String.valueOf(productById.get().getImage().length));
            resp.setHeader("Content-Disposition","inline; filename=\"" + productById.get().getName() + "\"");
            resp.getOutputStream().write(productById.get().getImage());
        }
    }
}
