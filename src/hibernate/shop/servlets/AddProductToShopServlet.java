package hibernate.shop.servlets;

import hibernate.hibernate.util.RepositoryUtil;
import hibernate.hibernate.util.UserSessionHelper;
import hibernate.shop.*;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.Optional;

/*
    create 2018-04-12 20:44 by Michał
*/

public class AddProductToShopServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {

        Optional<User> userFromCookie = UserSessionHelper.getUserFromCookie(req.getCookies());
        PrintWriter writer = resp.getWriter();

        if (userFromCookie.isPresent() && userFromCookie.get().getEmail().equals("admin@sda.pl")) {
            String name = req.getParameter("name");
            ProductType productType = ProductType.valueOf(req.getParameter("productType"));
            BigDecimal grossPrice = MathUtil.parseStringToBigDecimal(req.getParameter("grossPrice"));
            BigDecimal netPrice = MathUtil.parseStringToBigDecimal(req.getParameter("netPrice"));
            Price price = new Price(grossPrice, netPrice);
            Product product = new Product(name, productType, price);
            RepositoryUtil<Product> productRepositoryUtil = new RepositoryUtil<>();
            productRepositoryUtil.saveObjectIntoDB(product);
            String result = "Zapisano z id: " + product.getId();
            writer.write(result);

        }
    }
}
