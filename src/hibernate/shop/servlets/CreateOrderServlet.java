package hibernate.shop.servlets;

/*
    create 2018-04-14 09:44 by Michał
*/

import hibernate.hibernate.util.UserSessionHelper;
import hibernate.shop.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@WebServlet(name = "CreateOrderServlet", urlPatterns = "/createOrder")
public class CreateOrderServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Optional<User> userFromCookie = UserSessionHelper.getUserFromCookie(req.getCookies());
        if (userFromCookie.isPresent()) {
            Optional<Cart> cartByUserId = CartRepository.findByUserId(userFromCookie.get().getId());
            cartByUserId.ifPresent(this::createAndSaveOrder);
            req.getRequestDispatcher("/orderHistory.jsp").forward(req, resp);
        }
    }

    private void createAndSaveOrder(Cart cart) {
        //TODO convert into builder
        Order build = Order.builder()
                .totalGross(cart.getTotalGross())
                .totalNet(cart.getTotalNet())
                .user(cart.getUser())
                .build();

        Order order = new Order();
        order.setTotalGross(cart.getTotalGross());
        order.setTotalGross(cart.getTotalNet());
        order.setUser(cart.getUser());

        OrderHistory orderHistory = OrderHistory
                .builder()
                .statusDateTime(LocalDateTime.now())
                .orderStatus(OrderStatus.NEW)
                .build();

        Set<OrderHistory> orderHistorySet = new HashSet<>();

        order.setOrderHistorySet(orderHistorySet);
        order.addOrderHistory(orderHistory);

        order.setOrderItemSet(new HashSet<>());

        for (CartItem item : cart.getCartItemSet()) {
            OrderItem orderItem = OrderItem.builder()
                    .product(item.getProduct())
                    .amount(item.getAmount())
                    .price(item.getPrice())
                    .build();
            order.addOrderItem(orderItem);
        }
        OrderRepository.saveOrderIntoDB(order);
        CartRepository.removeCart(cart);
    }
}
