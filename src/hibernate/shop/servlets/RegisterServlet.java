package hibernate.shop.servlets;

import hibernate.shop.User;
import hibernate.shop.UserRepository;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

/*
    create 2018-04-10 11:48 by Michał
*/
public class RegisterServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        String passwordRepeat = req.getParameter("passwordRepeat");

        boolean isValid = true;

        //check if all form fields are filled
        if (firstName.isEmpty() || lastName.isEmpty() || email.isEmpty() || password.isEmpty() || passwordRepeat.isEmpty()) {
            System.out.println("Proszę uzupełłnić formularz!");
            isValid = false;
        }

        //check if password and passwordRepeat are equal
        if (!password.equals(passwordRepeat)) {
            System.out.println("Hasła są różne!");
            isValid = false;
        }

        //check if user with such email already exist
        Optional<User> userByEmail = UserRepository.findByEmail(email);
        if (userByEmail.isPresent()) {
            System.out.println("Użytkownik o takim adresie e-mail już istnieje w bazie!");
            isValid = false;
        }

        if (isValid) {
            //use builder from lombok
            User user = User.builder()
                    .firstName(firstName)
                    .lastName(lastName)
                    .email(email)
                    .password(password)
                    .build();
            UserRepository.saveUser(user);
        }
        req.getRequestDispatcher("/index.jsp").forward(req, resp);
    }
}
