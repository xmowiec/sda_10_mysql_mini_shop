# java5pozHibernate
Lista funkcjonalności:
1.	rejestracja użytkownika
2.	zarejestrowany użytkownik może się zalogować
3.	zarejestrowany użytkownik musi zweryfikować adres email
4.	zalogowany użytkownik może dodawać produkty do koszyka
5.	użytkownik anonimowy może przeglądać towary
6.	zalogowany użytkownik może złożyć zamówienie na podstawie koszyka
7.	zalogowany użytkownik może zgłosić reklamację do zakupionego towaru
8.	zalogowany użytkownik może ocenić zakupiony towar
9.	anonimowy użytkownik może dodać produkty do koszyka, ale musi się zalogować w celu złożenia zamówienia
10.	zalogowany użytkownik może napisać wiadomość do sklepu odnośnie zamówienia
11.	po zakupie towaru sklep powinien wysłać email
12.	zalogowany użytkownik może dodać produkt do listy ulubionych towarów
13.	administrator sklepu może dodać nowy/edytować produkt
14.	administrator sklepu może dodawać/zmieniać nowe kategorie
15.	administrator sklepu może odpowiadać wiadomości
16.	administrator sklepu akceptuję opinię towaru
17.	administrator sklepu może zablokować użytkownika
